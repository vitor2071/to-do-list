import 'package:flutter/material.dart';
import 'package:to_do_list/pages/home.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(), 
    );
  }
}