import 'package:flutter/material.dart';

class MyInputText extends StatelessWidget {
  const MyInputText({Key? key, this.vhintText, this.vlabelText}) : super(key: key);

  final String? vhintText; 
  final String? vlabelText; 
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
        hintText: vhintText, 
        labelText: vlabelText,
       ),
     );
  }
}