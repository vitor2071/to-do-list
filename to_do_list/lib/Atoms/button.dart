import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  const MyButton({Key? key, required this.vOnPressed, required this.iconButton }) : super(key: key);


final Function() vOnPressed; 
final Icon iconButton; 
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(onPressed: vOnPressed,
     child: iconButton);
  }
}