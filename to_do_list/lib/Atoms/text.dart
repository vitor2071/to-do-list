import 'package:flutter/material.dart';

class MyText extends StatelessWidget {
   const MyText( this.title, {Key? key, } ) : super(key: key);

  final String? title; 
  
  @override
  Widget build(BuildContext context) {
    return Text(title!);
  }
}