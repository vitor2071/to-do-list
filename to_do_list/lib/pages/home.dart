import 'package:flutter/material.dart';
import 'package:to_do_list/Atoms/button.dart';
import 'package:to_do_list/Atoms/input.dart';
import 'package:to_do_list/Atoms/text.dart';

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold( 
      body: Padding(
      
        padding: const EdgeInsets.all(8.0),
        child: Column( 
          mainAxisAlignment: MainAxisAlignment.start,
          children:  [ 
            const MyText("To-Do"),
            const MyInputText(
              vhintText: "Insira aqui sua tarefa",
            vlabelText: "Tarefa:",
            ),
            MyButton(
              vOnPressed: () {  }, 
            iconButton: const Icon(Icons.add_card_rounded),
            ),
          ],
        ),
      ),
    );
  }
}